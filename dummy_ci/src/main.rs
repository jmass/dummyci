fn main() {
    println!("Hello, world!");
}

#[test]
fn it_runs() {
    let mut cmd = assert_cmd::Command::cargo_bin("dummy_ci").unwrap();
    cmd.assert().success();
}
